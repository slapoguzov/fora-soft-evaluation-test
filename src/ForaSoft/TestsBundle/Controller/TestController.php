<?php

namespace ForaSoft\TestsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ForaSoft\TestsBundle\Entity\Attempt;

class TestController extends Controller
{
    public function indexAction()
    {
        return $this->render('ForaSoftTestsBundle:Test:index.html.twig');
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()
                   ->getManager();
        
        $test = $em->getRepository('ForaSoftTestsBundle:Test')->find($id);

        if(!$test)
            throw $this->createNotFoundException('Unable to find test.');
        
        return $this->render('ForaSoftTestsBundle:Test:show.html.twig', array(
            'test' => $test,
            'user' => $this->getUser()
        ));

    }

    public function resultsAction($id, Request $request)
    {
        if(!$request->isMethod($request::METHOD_POST))
            throw $this->createNotFoundException('Unsupported method');

        $user_answers = $request->request->get('quest');

        $em = $this->getDoctrine()
                   ->getManager();

        $test = $em->getRepository('ForaSoftTestsBundle:Test')->find($id);
        $answerRepository = $em->getRepository('ForaSoftTestsBundle:Answer');
        if(!$test)
            throw $this->createNotFoundException('Unable to find test.');

        $results = array();
        $countCorrectAnswer = 0;

        //check user answer
        foreach ($test->getQuestions() as $q => $question) {
            $results[$question->getId()] = array();
            if($question->getType() == 'textarea')
            {
                $results[$question->getId()][1] = $user_answers[$question->getId()][0];
                $answ = $answerRepository->findByText($user_answers[$question->getId()][0]);
                
                if(!$answ)
                {
                    $results[$question->getId()][0] = false;
                    continue;
                }

                $answ = $answ[0];

                if($answ && $answ->getQuest()->getId() == $question->getId())
                {
                    $results[$question->getId()][0] = true;
                    $countCorrectAnswer++;
                }
                else {
                    $results[$question->getId()][0] = false;
                }
                continue;
            }
            $userAnswerOnQuestIsCorrect = true;
            foreach ($question->getAnswers() as $a => $answer) {
                $userAnswerIsCorrect = false;
                if(!isset($user_answers[$question->getId()]))
                { 
                    $results[$question->getId()][$answer->getId()] = $userAnswerIsCorrect;
                    $userAnswerOnQuestIsCorrect = false;
                    continue;
                }
                foreach ($user_answers[$question->getId()] as $a_u => $answer_u) {
                    if($answer_u == $answer->getId())
                    {
                        $results[$question->getId()][$answer->getId()] = true;
                        if($answer->getIsCorrect()) {
                            $userAnswerIsCorrect = true;
                        }
                        else {
                            $userAnswerOnQuestIsCorrect = false;
                            $userAnswerIsCorrect = false;
                        }
                        unset($user_answers[$question->getId()][$a_u]);
                        break;
                    }
                }
                if(!isset($results[$question->getId()][$answer->getId()]) || !$results[$question->getId()][$answer->getId()])
                {
                    if($answer->getIsCorrect() )
                        $userAnswerOnQuestIsCorrect = false;
                    $results[$question->getId()][$answer->getId()] = false;
                }
            }
            if($userAnswerOnQuestIsCorrect)
                $countCorrectAnswer++;  

        }

        $user = $this->getUser();
        //save results
        $attempt = new Attempt();
        $attempt->setTest($test);
        $attempt->setCountCorrectAnswer($countCorrectAnswer);
        if($user)
             $attempt->setUser($user);
             
        $em->persist($attempt);
        $em->flush();
        return $this->render('ForaSoftTestsBundle:Test:result.html.twig', array(
            'user' => $user,
            'test' => $test,
            'countCorrectAnswer' => $countCorrectAnswer,
            'results' => $results,
            'numberQuests' => count($test->getQuestions())
        ));

    }

}