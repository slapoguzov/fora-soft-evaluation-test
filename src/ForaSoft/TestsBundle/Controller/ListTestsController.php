<?php

namespace ForaSoft\TestsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListTestsController extends Controller
{
    private $countTestsOnPage = 10;

    public function indexAction()
    {
        return $this->showAction(1);
    }

    public function showAction($number) 
    {
        $em = $this->getDoctrine()
                   ->getManager();

        $tests = $em->createQueryBuilder()
                    ->select('t')
                    ->from('ForaSoftTestsBundle:Test','t')
                    ->addOrderBy('t.created', 'DESC')
                    ->setFirstResult($this->countTestsOnPage * ($number-1))
                    ->setMaxResults($this->countTestsOnPage)
                    ->getQuery()
                    ->getResult();

        return $this->render('ForaSoftTestsBundle:ListTests:index.html.twig', array(
            'tests' => $tests,
            'user' => $this->getUser()
        ));
    }
}