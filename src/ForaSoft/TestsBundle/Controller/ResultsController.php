<?php

namespace ForaSoft\TestsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ForaSoft\TestsBundle\Entity\Attempt;

class ResultsController extends Controller
{

    public function showAction() 
    {
        $user = $this->getUser();
        
        if($user)
            $results = $user->getAttempts();

        return $this->render('ForaSoftTestsBundle:Results:show.html.twig', array(
            'results' => $results,
            'user' => $this->getUser()
        ));
    }
}