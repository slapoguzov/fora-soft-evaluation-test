<?php

namespace ForaSoft\TestsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ForaSoft\TestsBundle\Entity\Repository\AttemptRepository")
 * @ORM\Table(name="attempt")
 */
class Attempt
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="attempts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Test")
     * @ORM\JoinColumn(name="test_id", referencedColumnName="id")
     */
    protected $test;

    /**
     * @ORM\Column(type="integer")
     */
    protected $countCorrectAnswer;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;

    public function __construct()
    {
        $this->setDate(new \DateTime());
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countCorrectAnswer
     *
     * @param integer $countCorrectAnswer
     *
     * @return Attempt
     */
    public function setCountCorrectAnswer($countCorrectAnswer)
    {
        $this->countCorrectAnswer = $countCorrectAnswer;

        return $this;
    }

    /**
     * Get countCorrectAnswer
     *
     * @return integer
     */
    public function getCountCorrectAnswer()
    {
        return $this->countCorrectAnswer;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Attempt
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \ForaSoft\TestsBundle\Entity\User $user
     *
     * @return Attempt
     */
    public function setUser(\ForaSoft\TestsBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ForaSoft\TestsBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set test
     *
     * @param \ForaSoft\TestsBundle\Entity\Test $test
     *
     * @return Attempt
     */
    public function setTest(\ForaSoft\TestsBundle\Entity\Test $test = null)
    {
        $this->test = $test;

        return $this;
    }

    /**
     * Get test
     *
     * @return \ForaSoft\TestsBundle\Entity\Test
     */
    public function getTest()
    {
        return $this->test;
    }
}
