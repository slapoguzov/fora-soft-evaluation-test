<?php

namespace ForaSoft\TestsBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Attempt", mappedBy="user")
     */
    protected $attempts;

    public function __construct()
    {
        parent::__construct();
        // your own logic

        $this->attempts = new ArrayCollection();
    }

    /**
     * Add attempt
     *
     * @param \ForaSoft\TestsBundle\Entity\Attempt $attempt
     *
     * @return User
     */
    public function addAttempt(\ForaSoft\TestsBundle\Entity\Attempt $attempt)
    {
        $this->attempts[] = $attempt;

        return $this;
    }

    /**
     * Remove attempt
     *
     * @param \ForaSoft\TestsBundle\Entity\Attempt $attempt
     */
    public function removeAttempt(\ForaSoft\TestsBundle\Entity\Attempt $attempt)
    {
        $this->attempts->removeElement($attempt);
    }

    /**
     * Get attempts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttempts()
    {
        return $this->attempts;
    }
}
