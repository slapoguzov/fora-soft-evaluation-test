<?php

namespace ForaSoft\TestsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="ForaSoft\TestsBundle\Entity\Repository\QuestRepository")
 * @ORM\Table(name="quest")
 */
class Quest
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $text;
    
    /**
     * @ORM\ManyToOne(targetEntity="Test", inversedBy="questions")
     * @ORM\JoinColumn(name="test_id", referencedColumnName="id")
     */
    protected $test;

    /**
     * @ORM\Column(type="string", length=16)
     * textarea: free answer
     * radio: one correct
     * checkbox: few correct
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="quest")
     */
    protected $answers;

    function __construct() {

        $this->answers = new ArrayCollection();

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Quest
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Quest
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set test
     *
     * @param \ForaSoft\TestsBundle\Entity\Test $test
     *
     * @return Quest
     */
    public function setTest(\ForaSoft\TestsBundle\Entity\Test $test = null)
    {
        $this->test = $test;

        return $this;
    }

    /**
     * Get test
     *
     * @return \ForaSoft\TestsBundle\Entity\Test
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * Add answer
     *
     * @param \ForaSoft\TestsBundle\Entity\Answer $answer
     *
     * @return Quest
     */
    public function addAnswer(\ForaSoft\TestsBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \ForaSoft\TestsBundle\Entity\Answer $answer
     */
    public function removeAnswer(\ForaSoft\TestsBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}
