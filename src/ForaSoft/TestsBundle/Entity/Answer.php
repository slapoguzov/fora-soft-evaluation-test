<?php

namespace ForaSoft\TestsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ForaSoft\TestsBundle\Entity\Repository\AnswerRepository")
 * @ORM\Table(name="answer")
 */
class Answer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $text;
    
    /**
     * @ORM\ManyToOne(targetEntity="Quest", inversedBy="answers")
     * @ORM\JoinColumn(name="quest_id", referencedColumnName="id")
     */
    protected $quest;

    /**
     * @ORM\Column(type="boolean", length=1)
     */
    protected $isCorrect;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Answer
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set isCorrect
     *
     * @param string $isCorrect
     *
     * @return Answer
     */
    public function setIsCorrect($isCorrect)
    {
        $this->isCorrect = $isCorrect;

        return $this;
    }

    /**
     * Get isCorrect
     *
     * @return string
     */
    public function getIsCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * Set quest
     *
     * @param \ForaSoft\TestsBundle\Entity\Quest $quest
     *
     * @return Answer
     */
    public function setQuest(\ForaSoft\TestsBundle\Entity\Quest $quest = null)
    {
        $this->quest = $quest;

        return $this;
    }

    /**
     * Get quest
     *
     * @return \ForaSoft\TestsBundle\Entity\Quest
     */
    public function getQuest()
    {
        return $this->quest;
    }
}
