<?php

namespace ForaSoft\TestsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ForaSoft\TestsBundle\Entity\Test;

class TestFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager) {

        $test1 = new Test();
        $test1->setTitle("Тест на знание программирования");
        $test1->setDescription("Тест на знание программирования пожет вам понять на сколько хорошовы знаете программирование");
        $test1->setImage("http://lifestyle-drive.ru/wp-content/uploads/2012/02/test.jpg");
        $manager->persist($test1);

        $test2 = new Test();
        $test2->setTitle("Игра престолов");
        $test2->setDescription("«Игра престолов» - американский драматический телесериал в жанре фэнтези, созданный по мотивам цикла романов «Песнь Льда и Огня» писателя Джорджа Мартина. А вы смотрели этот сериал? Давайте проверим, насколько хорошо вы все запомнили.");
        $test2->setImage("http://tvsupernatural.ru/cs/2016/04/Igra-prestolov-6-sezon-wallpaper-2-730x350.jpg");
        $manager->persist($test2);
        
        $test3 = new Test();
        $test3->setTitle("Тест на знание городов и стран");
        $test3->setDescription("Профессиональная сертификация по программе «Семейство протоколов TCP/IP» является стандартом, подтверждающим высокую компетентность специалиста в вопросе администрирования локальных и глобальных IP-сетей");
        $test3->setImage("http://kvant.space/sites/default/files/earth.jpg");
        $manager->persist($test3);


        $manager->flush();
        
        $this->addReference('test-1', $test1);
        $this->addReference('test-2', $test2);
        $this->addReference('test-3', $test3);
              

    }

    public function getOrder()
    {
        return 1;
    }    
}
