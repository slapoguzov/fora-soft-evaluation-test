<?php

namespace ForaSoft\TestsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ForaSoft\TestsBundle\Entity\Test;
use ForaSoft\TestsBundle\Entity\Quest;

class QuestFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager) {

        $quest1_1 = new Quest();
        $quest1_1->setText("Что такое условие?");
        $quest1_1->setTest($manager->merge($this->getReference('test-1')));
        $quest1_1->setType("radio");
        $manager->persist($quest1_1);

        $quest1_2 = new Quest();
        $quest1_2->setText("Что такое цикл?");
        $quest1_2->setTest($manager->merge($this->getReference('test-1')));
        $quest1_2->setType("radio");
        $manager->persist($quest1_2);

        $quest1_3 = new Quest();
        $quest1_3->setText("Оператор условия иногда называют, оператором...");
        $quest1_3->setTest($manager->merge($this->getReference('test-1')));
        $quest1_3->setType("textarea");
        $manager->persist($quest1_3);

        $quest1_4 = new Quest();
        $quest1_4->setText("Какие бывают типы данных?");
        $quest1_4->setTest($manager->merge($this->getReference('test-1')));
        $quest1_4->setType("checkbox");
        $manager->persist($quest1_4);

        /*** TEST 2 ***/

        $quest2_1 = new Quest();
        $quest2_1->setText("Одно из имен Дейнерис Таргариен – Мать … ");
        $quest2_1->setTest($manager->merge($this->getReference('test-2')));
        $quest2_1->setType("radio");
        $manager->persist($quest2_1);

        $quest2_2 = new Quest();
        $quest2_2->setText("Настоящий отец Джофри?");
        $quest2_2->setTest($manager->merge($this->getReference('test-2')));
        $quest2_2->setType("radio");
        $manager->persist($quest2_2);

        $quest2_3 = new Quest();
        $quest2_3->setText("Детей Неда Старка звали... ");
        $quest2_3->setTest($manager->merge($this->getReference('test-2')));
        $quest2_3->setType("checkbox");
        $manager->persist($quest2_3);

        /*** TEST 3 ***/

        $quest3_1 = new Quest();
        $quest3_1->setText("В каком южноамериканском городе стоит 38-метровая статуя Иисуса Христа?");
        $quest3_1->setTest($manager->merge($this->getReference('test-3')));
        $quest3_1->setType("radio");
        $manager->persist($quest3_1);

        $quest3_2 = new Quest();
        $quest3_2->setText("Как иначе называют Нидерланды?");
        $quest3_2->setTest($manager->merge($this->getReference('test-3')));
        $quest3_2->setType("textarea");
        $manager->persist($quest3_2);

        $quest3_3 = new Quest();
        $quest3_3->setText("Как называется самый большой город Китая?");
        $quest3_3->setTest($manager->merge($this->getReference('test-3')));
        $quest3_3->setType("textarea");
        $manager->persist($quest3_3);

        $manager->flush();

        $this->addReference('quest-1-1', $quest1_1);
        $this->addReference('quest-1-2', $quest1_2);
        $this->addReference('quest-1-3', $quest1_3);
        $this->addReference('quest-1-4', $quest1_4);
        $this->addReference('quest-2-1', $quest2_1);
        $this->addReference('quest-2-2', $quest2_2);
        $this->addReference('quest-2-3', $quest2_3);
        $this->addReference('quest-3-1', $quest3_1);
        $this->addReference('quest-3-2', $quest3_2);
        $this->addReference('quest-3-3', $quest3_3);                    

    }

    public function getOrder()
    {
        return 2;
    }    
}
