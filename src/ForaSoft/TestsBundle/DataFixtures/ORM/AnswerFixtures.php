<?php

namespace ForaSoft\TestsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ForaSoft\TestsBundle\Entity\Quest;
use ForaSoft\TestsBundle\Entity\Answer;

class AnswerFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager) {

        /***** QUEST 1 *****/

        $answer1_1_1 = new Answer();
        $answer1_1_1->setText("Конструкция языка программирования, обеспечивающее выполнение действий только при выполнении некоторого логического выражения");
        $answer1_1_1->setQuest($manager->merge($this->getReference('quest-1-1')));
        $answer1_1_1->setIsCorrect(true);
        $manager->persist($answer1_1_1);

        $answer1_1_2 = new Answer();
        $answer1_1_2->setText("Некоторая часть исходного кода обеспечивающее повторение определенного действия");
        $answer1_1_2->setQuest($manager->merge($this->getReference('quest-1-1')));
        $answer1_1_2->setIsCorrect(false);
        $manager->persist($answer1_1_2);

        $answer1_1_3 = new Answer();
        $answer1_1_3->setText("Эээ, ну это когда жена говорит \"Либо я покупаю шубу, либо уезжаю к маме!\"");
        $answer1_1_3->setQuest($manager->merge($this->getReference('quest-1-1')));
        $answer1_1_3->setIsCorrect(false);
        $manager->persist($answer1_1_3);

        /***** QUEST 2 *****/

        $answer1_2_1 = new Answer();
        $answer1_2_1->setText("Имя переменной");
        $answer1_2_1->setQuest($manager->merge($this->getReference('quest-1-2')));
        $answer1_2_1->setIsCorrect(false);
        $manager->persist($answer1_2_1);

        $answer1_2_2 = new Answer();
        $answer1_2_2->setText("Оператор присваивания");
        $answer1_2_2->setQuest($manager->merge($this->getReference('quest-1-2')));
        $answer1_2_2->setIsCorrect(false);
        $manager->persist($answer1_2_2);

        $answer1_2_3 = new Answer();
        $answer1_2_3->setText("Оператор предназначенный для многократного использования определенных инструкций");
        $answer1_2_3->setQuest($manager->merge($this->getReference('quest-1-2')));
        $answer1_2_3->setIsCorrect(true);
        $manager->persist($answer1_2_3);
        
        /***** QUEST 3 *****/

        $answer1_3_1 = new Answer();
        $answer1_3_1->setText("ветвления");
        $answer1_3_1->setQuest($manager->merge($this->getReference('quest-1-3')));
        $answer1_3_1->setIsCorrect(true);
        $manager->persist($answer1_3_1);

        /***** QUEST 4 *****/

        $answer1_4_1 = new Answer();
        $answer1_4_1->setText("Космические");
        $answer1_4_1->setQuest($manager->merge($this->getReference('quest-1-4')));
        $answer1_4_1->setIsCorrect(false);
        $manager->persist($answer1_4_1);

        $answer1_4_2 = new Answer();
        $answer1_4_2->setText("Целочисленные");
        $answer1_4_2->setQuest($manager->merge($this->getReference('quest-1-4')));
        $answer1_4_2->setIsCorrect(true);
        $manager->persist($answer1_4_2);

        $answer1_4_3 = new Answer();
        $answer1_4_3->setText("С плавающей точкой");
        $answer1_4_3->setQuest($manager->merge($this->getReference('quest-1-4')));
        $answer1_4_3->setIsCorrect(true);
        $manager->persist($answer1_4_3);

        $answer1_4_4 = new Answer();
        $answer1_4_4->setText("Порциональные");
        $answer1_4_4->setQuest($manager->merge($this->getReference('quest-1-4')));
        $answer1_4_4->setIsCorrect(false);
        $manager->persist($answer1_4_4);

        /***** TEST 2 *****/
        /***** QUEST 1 *****/
        $answer2_1_1 = new Answer();
        $answer2_1_1->setText("Ящериц");
        $answer2_1_1->setQuest($manager->merge($this->getReference('quest-2-1')));
        $answer2_1_1->setIsCorrect(false);
        $manager->persist($answer2_1_1);

        $answer2_1_2 = new Answer();
        $answer2_1_2->setText("Драконов");
        $answer2_1_2->setQuest($manager->merge($this->getReference('quest-2-1')));
        $answer2_1_2->setIsCorrect(true);
        $manager->persist($answer2_1_2);

        $answer2_1_3 = new Answer();
        $answer2_1_3->setText("Саламандр");
        $answer2_1_3->setQuest($manager->merge($this->getReference('quest-2-1')));
        $answer2_1_3->setIsCorrect(false);
        $manager->persist($answer2_1_3);

         /***** QUEST 2 *****/
        $answer2_2_1 = new Answer();
        $answer2_2_1->setText("Роберт Баратеон");
        $answer2_2_1->setQuest($manager->merge($this->getReference('quest-2-2')));
        $answer2_2_1->setIsCorrect(false);
        $manager->persist($answer2_2_1);

        $answer2_2_2 = new Answer();
        $answer2_2_2->setText("Джон Аррен");
        $answer2_2_2->setQuest($manager->merge($this->getReference('quest-2-2')));
        $answer2_2_2->setIsCorrect(false);
        $manager->persist($answer2_2_2);

        $answer2_2_3 = new Answer();
        $answer2_2_3->setText("Джейме Ланнистер");
        $answer2_2_3->setQuest($manager->merge($this->getReference('quest-2-2')));
        $answer2_2_3->setIsCorrect(true);
        $manager->persist($answer2_2_3);
                       

        /***** QUEST 3 *****/

        $answer2_3_1 = new Answer();
        $answer2_3_1->setText("Робб");
        $answer2_3_1->setQuest($manager->merge($this->getReference('quest-2-3')));
        $answer2_3_1->setIsCorrect(true);
        $manager->persist($answer2_3_1);

        $answer2_3_2 = new Answer();
        $answer2_3_2->setText("Ширен");
        $answer2_3_2->setQuest($manager->merge($this->getReference('quest-2-3')));
        $answer2_3_2->setIsCorrect(false);
        $manager->persist($answer2_3_2);

        $answer2_3_3 = new Answer();
        $answer2_3_3->setText("Санса");
        $answer2_3_3->setQuest($manager->merge($this->getReference('quest-2-3')));
        $answer2_3_3->setIsCorrect(true);
        $manager->persist($answer2_3_3);

        $answer2_3_4 = new Answer();
        $answer2_3_4->setText("Киван");
        $answer2_3_4->setQuest($manager->merge($this->getReference('quest-2-3')));
        $answer2_3_4->setIsCorrect(false);
        $manager->persist($answer2_3_4);

        $answer2_3_5 = new Answer();
        $answer2_3_5->setText("Арья");
        $answer2_3_5->setQuest($manager->merge($this->getReference('quest-2-3')));
        $answer2_3_5->setIsCorrect(true);
        $manager->persist($answer2_3_5);

        $answer2_3_6 = new Answer();
        $answer2_3_6->setText("Джон");
        $answer2_3_6->setQuest($manager->merge($this->getReference('quest-2-3')));
        $answer2_3_6->setIsCorrect(true);
        $manager->persist($answer2_3_6);

        /***** TEST 3 *****/
        /***** QUEST 1 *****/
        
        $answer3_1_1 = new Answer();
        $answer3_1_1->setText("Бразилиа");
        $answer3_1_1->setQuest($manager->merge($this->getReference('quest-3-1')));
        $answer3_1_1->setIsCorrect(false);
        $manager->persist($answer3_1_1);

        $answer3_1_2 = new Answer();
        $answer3_1_2->setText("Буэнос-Айрес");
        $answer3_1_2->setQuest($manager->merge($this->getReference('quest-3-1')));
        $answer3_1_2->setIsCorrect(false);
        $manager->persist($answer3_1_2);

        $answer3_1_3 = new Answer();
        $answer3_1_3->setText("Рио-де-Жанейро");
        $answer3_1_3->setQuest($manager->merge($this->getReference('quest-3-1')));
        $answer3_1_3->setIsCorrect(true);
        $manager->persist($answer3_1_3);

        /***** QUEST 2 *****/
        $answer3_2_1 = new Answer();
        $answer3_2_1->setText("Голландия");
        $answer3_2_1->setQuest($manager->merge($this->getReference('quest-3-2')));
        $answer3_2_1->setIsCorrect(true);
        $manager->persist($answer3_2_1);

        /***** QUEST 3 *****/
        $answer3_3_1 = new Answer();
        $answer3_3_1->setText("Шанхай");
        $answer3_3_1->setQuest($manager->merge($this->getReference('quest-3-3')));
        $answer3_3_1->setIsCorrect(true);
        $manager->persist($answer3_3_1);

        $manager->flush(); 
    }

    public function getOrder()
    {
        return 3;
    }    
}
