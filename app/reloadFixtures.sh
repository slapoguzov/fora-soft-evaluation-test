#!/bin/sh

# полный путь до скрипта
ABSOLUTE_FILENAME=`readlink -e "$0"`
# каталог в котором лежит скрипт
DIRECTORY=`dirname "$ABSOLUTE_FILENAME"`

php $DIRECTORY/console doctrine:database:drop --force
php $DIRECTORY/console doctrine:database:create
php $DIRECTORY/console doctrine:schema:update --force
php $DIRECTORY/console doctrine:fixtures:load --append